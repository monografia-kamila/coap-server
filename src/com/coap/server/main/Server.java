/*******************************************************************************
 * Licenced under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.coap.server.main;

import java.net.SocketException;

import org.eclipse.californium.core.CoapResource;
import org.eclipse.californium.core.CoapServer;
import org.eclipse.californium.core.coap.CoAP.ResponseCode;
import org.eclipse.californium.core.server.resources.CoapExchange;


/**
 * The Class CoAPServer.
 * @author Kamila Serpa (http://kamilaserpa.github.io)
 */
public class Server extends CoapServer {

	FirebaseService fs = new FirebaseService();
	
	public static void main(String[] args) {		
		
		try {
			// create server
			Server server = new Server();
			server.start();
		} catch (SocketException e) {
			System.err.println("Failed to initialize server: " + e.getMessage());
		}
	}

	/**
	 * Instantiates a new CoAP server.
	 * @throws SocketException the socket exception
	 */
	public Server() throws SocketException {

		// provide an instance of a resource
		add(new TemperaturaResource());
	}

	/**
	 * The Class PublishResource.
	 */
	class TemperaturaResource extends CoapResource {

		
		/**
		 * Instantiates a new temperatura resource.
		 */
		public TemperaturaResource() {
			// set resource identifier
			super("temperatura");
			// set display name
			getAttributes().setTitle("Temperatura Resource");
		}

		String temperatura;
		String umidade;
		
		/* (non-Javadoc)
		 * @see org.eclipse.californium.core.CoapResource#handlePOST(org.eclipse.californium.core.server.resources.CoapExchange)
		 */		
		public void handlePOST(CoapExchange exchange) {	
			String str = exchange.getRequestText();
			String[] arrOfStr = str.split("/", 2);
			temperatura = arrOfStr[0];
			umidade = arrOfStr[1];
			System.out.println("Received - temperatura: " + temperatura + ", umidade: " + umidade);		
			try {
				/* Send data for Firebase */
				fs.writeData(temperatura, umidade);
				exchange.respond(ResponseCode.CREATED, temperatura);
			} catch (Exception e) {
				e.printStackTrace();
				exchange.respond(ResponseCode.BAD_REQUEST);
			}		
		}
		
		/* (non-Javadoc)
		 * @see org.eclipse.californium.core.CoapResource#handleGET(org.eclipse.californium.core.server.resources.CoapExchange)
		 */
		public void handleGET(CoapExchange exchange) {
			exchange.respond("<GET_REQUEST_SUCCESS> Temperatura: " + temperatura + " C" + "Umidade: " + umidade + "%");
		}
	}

}