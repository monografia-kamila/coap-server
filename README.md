# Coap Server Java

Java project that implements Server using the CoAP protocol. Receives data captured by a sensor and saved to Firebase. Uses the library [Eclipse Californium](https://www.eclipse.org/californium/).

## Getting Started

Download and update maven dependencies. Run the Server class with Java application.

### Eclipse Californium

<img src="https://www.eclipse.org/californium/images/logos/Cf_256.png" alt="californnium" width="40" style="float: right;"/>

Eclipse Californium is a Java implementation of [RFC7252 - Constrained Application Protocol](http://tools.ietf.org/html/rfc7252) for IoT Cloud services. Thus, the focus is on scalability and usability instead of resource-efficiency
like for embedded devices. Yet Californium is also suitable for embedded JVMs.

More information can be found at
[http://www.eclipse.org/californium/](http://www.eclipse.org/californium/)
and [http://coap.technology/](http://coap.technology/).


### Firebase

[Firebase](https://firebase.google.com) provides the tools and infrastructure
you need to develop apps, grow your user base, and earn money. The Firebase
Admin Java SDK enables access to Firebase services from privileged environments
(such as servers or cloud) in Java. Currently this SDK provides
Firebase custom authentication support, and Firebase realtime database access.

For more information, visit the
[Firebase Admin SDK setup guide](https://firebase.google.com/docs/admin/setup/).

To authenticate a service account and authorize it to access Firebase services, generate a private key file in JSON format. To generate a service account private key file, follow these steps:
 - In the Firebase Console, open Settings> Service Accounts.
 - Click Generate New Private Key and select Generate Key to Confirm.
 - Securely store the *JSON* file containing the key.

## Testing

Requests can be tested through the android coap client [SpitFirefox App](https://github.com/okleine/spitfirefox).

## Author

**Kamila Serpa**
* [Linked In](https://www.linkedin.com/in/kamila-serpa/)
* [Resume](https://kamilaserpa.github.io/)

## Built With

* [Sensor ESP8266 ESP-12](https://gitlab.com/monografia-kamila/coap-esp8266)
* [Front-end Angular](https://github.com/kamilaserpa/bluetooth-web-firestore) shows captured data
* [Java Coap Server](https://gitlab.com/monografia-kamila/coap-server) - This project
